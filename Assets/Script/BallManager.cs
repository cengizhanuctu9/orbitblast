﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallManager : MonoBehaviour
{
   // icerisinde bulundugumuz cemberin is triger olduguna dikkat edelim 
    Rigidbody2D rb;
    Vector2 vec=new Vector2(0,2);
    [SerializeField]
    GameObject center;
    [SerializeField]
    float hiz;
    private bool tiklandi;
    void Start()
    {
        tiklandi = true;
        rb = GetComponent<Rigidbody2D>();
    }

   
    void Update()
    { 
        if (Input.GetMouseButtonDown(0) && tiklandi)// eger down olmasaydi  topa fareye baildigi surece kuvvet uygulayacaktı
        {
            tiklandi = false;
            transform.parent = null;// kucuk topu cocukluktan cikardik

            Vector2 vec = center.transform.position - transform.position;// bileske vektor ike kuvet uyguladik
            rb.AddForce(-hiz*vec.normalized,ForceMode2D.Force);

            CenterManager.speed *= -1;// statick sayesinde nesne uretmeden merkez topun yonunu degistiridk
        }
       


    }
    private void OnTriggerExit2D(Collider2D collision)
    {

        if (collision.CompareTag("discember"))
        {
            rb.velocity = Vector2.zero;// hizi sıfirliyoruz ki top cemberin dışına fırmalmasın 

            Vector2 vec = center.transform.position - transform.position;// topa merkeze dogru ters kuvet uyguluyoruz
            rb.AddForce(140 * vec.normalized,ForceMode2D.Force);

        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("center"))         
        {
            tiklandi = true;// hatayi cozmek icin sadece temas halinde iken tiklayabilecegiz 
            transform.parent = collision.gameObject.transform;

        }
    }
}
