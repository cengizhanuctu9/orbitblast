﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParaManager : MonoBehaviour
{
    private float Radius = 2.2f;// buyuk cemberimizin capi
    public GameObject center;
    private Vector2 _centre;
    // private float RotateSpeed = 0f;// hizi arttirdikca top ekseninde donuyor
    // private float _angle;

    private void Start()
    {
        _centre = center.transform.position;

        float radsin = Random.Range(0, 30f);
        var offset = new Vector2(Mathf.Sin(radsin), Mathf.Cos(radsin)) * Radius;// yeni yem merkez cemberimizin
                                                                                // belirlediğimiz uzakliginda rasgele bir yere dogar
        transform.position = _centre + offset;
    }

    private void Update()
    {

        //_angle += RotateSpeed * Time.deltaTime;
        //var offset = new Vector2(Mathf.Sin(_angle), Mathf.Cos(_angle)) * Radius;
        //transform.position = _centre + offset;
    }
}
